# Automated deployment logic for Paroli's Freeswitch server

Requires ansible. If running on Windows, I'd recommend running from a WSL instance (e.g. Ubuntu). Vagrant file included for local development. Point towards a server running Debian >= 10

1) Install Ansible:

```
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
```

2) Update the included staging and inventory files with valid parameters.

FreeSwitch now required a SignalWire authentication token to download. Follow instructions [here](https://freeswitch.org/confluence/display/FREESWITCH/HOWTO+Create+a+SignalWire+Personal+Access+Token) and add yours to the inventory.

3) Run with:

```
ansible-playbook  mainplaybook.yml
```

To only update the app from Git and relaunch it (without going through the FreeSwitch, Node and LetsEncrypt setup):

```
ansible-playbook mainplaybook.yml --tags "paroli,ufw"
```

To skip a step:

```
ansible-playbook mainplaybook.yml --skip-tags "letsencrypt"
```
